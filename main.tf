resource "tls_private_key" "root" {
  count = var.prod_deployment ? 0 : 1

  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "root_cert" {
  key_algorithm   = "RSA"
  private_key_pem = var.prod_deployment ? var.prod_private_key_pem : tls_private_key.root[0].private_key_pem

  subject {
    common_name         = var.common_name
    organization        = var.organization
    organizational_unit = var.organizational_unit
    street_address      = var.street_address
    locality            = var.locality
    province            = var.province
    country             = var.country
    postal_code         = var.postal_code
  }

  # Do the math for me, years * days * hours
  validity_period_hours = var.validity_years * 365 * 24
  allowed_uses = var.allowed_uses
  is_ca_certificate   = true
}

