# Create cert-manager resources
resource "helm_release" "this" {
  count = var.enable_certmgr ? 1 : 0

  name       = var.certmgr_name
  repository = "jetstack"
  chart      = "cert-manager"
  namespace  = var.namespace
  # version         = var.certmgr_version
  reuse_values    = var.reuse_values
  force_update    = var.force_update
  recreate_pods   = var.recreate_pods
  cleanup_on_fail = var.cleanup_on_fail
  values          = var.values
  lint            = var.lint_chart

  set {
    name  = "installCRDs"
    value = true
  }
}

resource "kubernetes_namespace" "this" {
  # if enable_certmgr is true, and create_namespace is true, and namespace isn't null, make sure it's not "default" before creating it
  count = var.enable_certmgr ? (var.create_namespace ? (var.namespace != null ? (var.namespace != "default" ? 1 : 0) : 0) : 0) : 0

  metadata {
    annotations = {
      name = var.namespace
    }

    name = var.namespace
  }
}

# If prod_deployment == true, it checks prod_private_key string to ensure it's not null (it is by default) and applies it here, else if it's non-prod just use the key from remote state
resource "kubernetes_secret" "root_ca" {
  metadata {
    name      = "root-ca-keypair"
    namespace = var.namespace
  }

  data = {
    "tls.crt" = tls_self_signed_cert.root_cert.cert_pem
    "tls.key" = var.prod_deployment ? var.prod_private_key_pem != null ? var.prod_private_key_pem : "Key cannot be null!" : tls_private_key.root[0].private_key_pem
  }

  type = "kubernetes.io/tls"
}

# Deploy Cluster Issuer CRD with configured CA
resource "null_resource" "ca_cluster_issuer" {
  count = local.use_root_ca

  depends_on = [
    kubernetes_secret.root_ca,
    helm_release.this
  ]

  provisioner "local-exec" {
    command = "echo \"${data.template_file.certmgr_ca.rendered}\" | kubectl apply -f -"
    when    = create
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.certmgr_ca.rendered}\" | kubectl delete -f -"
    when    = destroy
  }
}
