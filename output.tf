# We don't want to save the Prod CA private key to state, but it cannot be recovered.
output "tls_private_key_pem" {
  value       = var.prod_deployment ? null : tls_private_key.root[0].private_key_pem
  description = "The private key in PEM format"
  sensitive   = true
}

output "tls_public_key_pem" {
  value       = tls_self_signed_cert.root_cert.cert_pem
  description = "The certificate data in PEM format"
}

output "validity_start_time" {
  value       = tls_self_signed_cert.root_cert.validity_start_time
  description = "When the certificate became valid"
}

output "validity_end_time" {
  value       = tls_self_signed_cert.root_cert.validity_end_time
  description = "When the certificate expires"
}
