data "template_file" "certmgr_ca" {
  template = file("${path.cwd}/templates/cluster_issuer.yaml")
  vars = {
    NAMESPACE   = var.namespace
    TYPE        = "ca"
    SECRET_NAME = "root-ca-keypair"
  }
}
